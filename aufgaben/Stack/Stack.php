<?php

declare(strict_types=1);

namespace Aufgaben\Stack;

/**
 * Erzeuge einen Stack.
 *
 * Wenn der Stack leer ist sollen die pop- und peek-Methiden null zurückgeben.
 *
 * @method void push(mixed $value)
 * @method mixed|null pop()
 * @method mixed|null peek()
 * @example $queue = new Stack();
 * $queue->push(1);
 * $queue->push(2);
 * $queue->peek() === 1;
 * $queue->pop() === 1;
 */
final class Stack
{
}
