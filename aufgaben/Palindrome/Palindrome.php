<?php

declare(strict_types=1);

namespace Aufgaben\Palindrome;

/**
 * Ein Palindrom ist ein String, der rückwärts gelesen denselben String ergibt.
 * Nich-alphanumerische Zeichen müssen ebenfalls unterstützt werden.
 *
 * @method static bool check(string $string)
 * @example Palindrome::check('asddsa')  === true
 * @example Palindrome::check('asdd')  === false
 */
final class Palindrome
{
}
