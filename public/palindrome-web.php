<!DOCTYPE html>
<html lang="de">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <title>Aufgabenbereich 1: Palindrom</title>
    <!-- Template  CSS File -->
    <link href="mystyle.css" rel="stylesheet">
</head>

<body>
    <h1>Aufgabenbereich 1: Palindrom</h1>
<div class="content">
<form method="post">        
    Eingabe Palindrom: <input type="text" name="text"/><br>  
    <!-- Eingabe kann beliebig sein: Buchstaben, Zahlen, Zeichnfolgen.. -->
    <input type="submit" value="Ist es ein Palindrom?"> 
    </form>  
    <?php   
        if($_POST)  
        {  
            $text = $_POST['text'];  
            //Eingabe wird umgekehrt:
            $reverse = strrev($text);  
              
            //Wort wird überprüft ob es umgekehrt gleich ist:  
            if($text == $reverse){  //Wenn ja Ausgabe:
                echo "Deine Eingabe '$text' ist ein Palindrom";     
            }else{  //Wenn nicht Ausgabe:
                echo "Deine Eingabe '$text' ist kein Palindrom";   
            }  
    }     
          ?>  
</body>
  
</html>