Anbei die Webversion vom Aufgabenbreich 1: Palindrom und Reverse mit Input-Funktion.
Ich habe zum Testen der Webversionen /localhost über xampp benutzt.

Leider habe ich es nicht mehr geschafft die PHP-Funktionen im A1 korrekt auszuführen. Aus diesem Grund lasse ich meine alternative Lösung 
für A1 in public:  Palindrom und Reverse.

Vielen Dank für eure Zeit und die Aufgaben! Es hat mir sehr viel Spaß gemacht sie zu bearbeiten und zu lösen. :)

Ich wünsche euch ein schönes Wochenende!

Liebe Grüße,
Büsra