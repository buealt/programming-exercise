<!DOCTYPE html>
<html lang="de">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <title>Aufgabenbereich 1: Reverse</title>
    <!-- Template  CSS File -->
    <link href="mystyle.css" rel="stylesheet">
</head>

<body>
    <h1>Aufgabenbereich 1: Reverse</h1>
<div class="content">
<form method="post">        
    Eingabe: <input type="text" name="text"/><br>  
    <input type="submit" value="Die Eingabe umkehren"> 
    </form>  
    <?php   
        if($_POST)  
        {  
            $text = $_POST['text'];  
            //Eingabe wird umgekehrt:
            $reverse = strrev($text);  
              
            //Wort wird umgekehrt und ausgegeben:  
                echo "Deine Eingabe umgekehrt:'$reverse' ";  
    }     
          ?>  
</body>
  
</html>