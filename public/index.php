<!DOCTYPE html>
<html lang="de">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <title>Aufgabenbreich 2</title>
    <!-- Template CSS File -->
    <link href="mystyle.css" rel="stylesheet">
</head>

<body>
<div class="content">
<h1>Aufgabenbereich 2</h1>

<form name="Userreg" action="nutzerdaten.php" method="get" onSubmit="return eingabeCheck()">
Benutzername: <input type="text" id="feld" name="eingabe"> <br><!--Benutzername-->
Passwort: <input type="password" id="feld" name="passwort">  <br><!--Passwort-->
Passwort bestätigen: <input type="password" id="feld" name="passwortcheck"> <br> <!--Passwort bestätigen-->
<input type="submit" value="Senden!">

</form>
</div>
<!-- Script -->
<script>
  function eingabeCheck()
{
 if(document.Userreg.eingabe.value == "")  { //Wenn Benutzerfeld leer ist: Fehleranzeige
   alert("Bitte Benutzernamen eingeben.");
   document.Userreg.firstname.focus();
   return false;
  }
 if(document.Userreg.passwort.value == "") { //Wenn Passwortfeld leer ist: Fehleranzeige
   alert("Passwort wurde nicht eingegeben.");
   document.Userreg.passwort.focus();
   return false;
  }
if(document.Userreg.passwort.value != document.Userreg.passwortcheck.value) { //wenn Eingabe der Passwörter nicht übereinstimmen wird eine Fehlermeldung zurückgegeben
   alert("Die Passwörter stimmen nicht überein. Bitte Passwort erneut eingeben.");
   document.Userreg.passwort.focus();
   return false;
  }
}
</script>
</body>
        <!--   End Body -->
</html>