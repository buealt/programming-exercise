<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "myDB";

$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error) {
  die("Verbindung fehlgeschlagen: " . $conn->connect_error);
}

// Tabelle erstellen in SQL
$sql = "CREATE TABLE Benutzerdaten (
id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
benutzername VARCHAR(30) NOT NULL,
passwort VARCHAR(30) NOT NULL,
reg_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
)";

if ($conn->query($sql) === TRUE) {
  echo "Tabelle Benutzerdaten wurde erfolgreich erstellt!";
} else {
  echo "Tabellenerstellung Benutzerdaten fehlgeschlagen: " . $conn->error;
}

$conn->close();
?> 