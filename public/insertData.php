<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "myDB";

$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error) {
  die("Verbindung fehlgeschlagen: " . $conn->connect_error);
}

$sql = "INSERT INTO Benutzerdaten (benutzername, passwort)
VALUES ('Testuser', 'HalloWelt')";

if ($conn->query($sql) === TRUE) {
  echo "Neuer Eintrag war erfolgreich!!";
} else {
  echo "Fehler: " . $sql . "<br>" . $conn->error;
}

$conn->close();
?> 