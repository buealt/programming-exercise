<?php
$servername = "localhost";
$username = "root";
$password = "";

//Verbindung erstellen
$conn = new mysqli($servername, $username, $password);
// Verbindung überprüfen
if ($conn->connect_error) {
  die("Verbindung fehlgeschlagen: " . $conn->connect_error);
}

// Datenbank erstellen
$sql = "CREATE DATABASE myDB";
if ($conn->query($sql) === TRUE) {
  echo "Datenbank wurde erfolgreich erstellt";
} else {
  echo "Fehler bei Datenbankerstellung: " . $conn->error;
}

$conn->close();
?> 